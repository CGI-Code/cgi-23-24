precision highp float;

uniform bool uUseNormals;
uniform sampler2D texture;

varying vec3 fNormal;
varying vec2 fTexCoord;

void main()
{
    vec3 c = vec3(1.0, 1.0, 1.0);

    if( uUseNormals) 
        c = 0.5 *(fNormal + vec3(1.0, 1.0, 1.0));

    gl_FragColor = vec4(c, 1.0) * texture2D(texture, fTexCoord);
}